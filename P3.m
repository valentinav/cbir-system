a= [0 1 2 3 0 1;1 2 3 0 1 2;2 3 0 1 2 3;3 0 1 2 3 0;0 1 2 3 0 1; 1 2 3 0 1 2];
matrizcoo=graycomatrix(a,'NumLevels',4,'GrayLimits',[]); % 4 niveles de gris
matrizcoo2=graycomatrix(a,'NumLevels',4,'GrayLimits',[],'Offset',[0 1;0 2]); %con qué orientaciones y a qué distancia
stats1 = graycoprops(matrizcoo2);
matrizcooD1= matrizcoo2(:,:,1);
matrizcooD2= matrizcoo2(:,:,2);
statsD1 = graycoprops(matrizcooD1, {'contrast','homogeneity'});

ROI= imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP3\ROI.jpg');
imshow(ROI);
GLCM = graycomatrix(ROI,'Offset',[0 1;-1 1;-1 0;-1 -1]); %angles "offset":(0,45,90,135)
stats=graycoprops(GLCM,{'Contrast','Homogeneity','Correlation','Energy'});
contrast=(stats.Contrast);
en=(stats.Energy);
co=(stats.Correlation);
hom=(stats.Homogeneity);
m=mean(mean(ROI)); 
s=std2((ROI));
f1=[m s hom co en contrast];
f1norm = f1/norm(f1);

GLCM2 = graycomatrix(ROI,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
figure(1);imshow(GLCM2(:,:,1));
figure(2);imshow(GLCM2(:,:,2));
figure(3);imshow(GLCM2(:,:,3));
figure(4);imshow(GLCM2(:,:,4));

%H. Trabajando con otras imágenes:
ROI= imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP3\ROI.jpg');
ROItum= imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP3\ROI_TUM.png');
ROIvert= imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP3\ROI_VERT.jpg');
ROIvertpng= imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP3\ROI_VERT.png');
figure; imshow(ROItum);
figure; imshow(ROIvert);
figure; imshow(ROIvertpng);
figure; imshow(ROI);
%Hay diferencia al realizar este proceso con ROI_VERT.png?
ROIvert1= ROIvert(:,:,1);
ROIvert2= ROIvert(:,:,2);
ROIvert3= ROIvert(:,:,3);
%Hay alguna diferencia entre ROIvert1, ROIvert2 y ROIvert3?
GLCMvert = graycomatrix(ROIvert1,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
ROItum1= ROItum(:,:,1);
ROItum2= ROItum(:,:,2);
ROItum3= ROItum(:,:,3);
GLCMtum = graycomatrix(ROItum1,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
GLCM2 = graycomatrix(ROI,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
GLCMvertpng = graycomatrix(ROIvertpng,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);

%I. Visualización de una de las matrices de coocurrencia de las tres regiones de interés.
figure;imshow(GLCM2(:,:,1));
figure;imshow(GLCMtum(:,:,1));
figure;imshow(GLCMvert(:,:,1));
figure;imshow(GLCMvertpng(:,:,1));

stats=graycoprops(GLCM2);
statsvert=graycoprops(GLCMvert);
statsvertpng=graycoprops(GLCMvertpng);
statstum=graycoprops(GLCMtum);

featuresGLCM2 = [stats.Contrast stats.Correlation stats.Energy stats.Homogeneity];
featuresGLCMvert = [statsvert.Contrast statsvert.Correlation statsvert.Energy statsvert.Homogeneity];
featuresGLCMvpng = [statsvertpng.Contrast statsvertpng.Correlation statsvertpng.Energy statsvertpng.Homogeneity];
featuresGLCMtum = [statstum.Contrast statstum.Correlation statstum.Energy statstum.Homogeneity];

FnormGLCM2 = featuresGLCM2/norm(featuresGLCM2);
FnormGLCMvert = featuresGLCMvert/norm(featuresGLCMvert);
FnormGLCMtum = featuresGLCMtum/norm(featuresGLCMtum);
FnormGLCMvertpng = featuresGLCMvpng/norm(featuresGLCMvpng);
