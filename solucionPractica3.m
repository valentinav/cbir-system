c1_read = dir('C:\Users\valen\Documents\2020.1\CBIR\practica3\conjunto1\*.jpeg');
c2_read = dir('C:\Users\valen\Documents\2020.1\CBIR\practica3\conjunto2\*.jpg');

%lee conjunto 1
for i = 1:length(c1_read)
    c1I = c1_read(i).name;
    c1namei='C:\Users\valen\Documents\2020.1\CBIR\practica3\conjunto1\';
    c1_IM{i} = imread(strcat(c1namei,c1I));
end

%lee conjunto 2
for i = 1:length(c2_read)
    c2I = c2_read(i).name;
    c2namei='C:\Users\valen\Documents\2020.1\CBIR\practica3\conjunto2\';
    c2_IM{i} = imread(strcat(c2namei,c2I));
end

%obtención de estadísticas y vector de caracteristicas a partir de GLCM conjunto 1
for i = 1:length(c1_read)
    c1I=rgb2gray(c1_IM{i});
    c1_glcms{i} = graycomatrix(c1I,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
    c1_stast{i} = graycoprops(c1_glcms{i},{'all'});
    c1_Vector{i}=[c1_stast{i}.Contrast c1_stast{i}.Correlation c1_stast{i}.Energy c1_stast{i}.Homogeneity];
    c1_normalizedVector{i} = c1_Vector{i}/norm(c1_Vector{i});
end

%obtención de estadísticas y vector de caracteristicas a partir de GLCM conjunto 2
for i = 1:length(c2_read)
    c2I=rgb2gray(c2_IM{i});
    c2_glcms{i} = graycomatrix(c2I,'NumLevels',256,'GrayLimits',[],'Offset',[0 1;-1 1;-1 0;-1 -1]);
    c2_stast{i} = graycoprops(c2_glcms{i},{'all'});
    c2_Vector{i}=[c2_stast{i}.Contrast c2_stast{i}.Correlation c2_stast{i}.Energy c2_stast{i}.Homogeneity];
    c2_normalizedVector{i} = c2_Vector{i}/norm(c2_Vector{i});
end

featuresIma = c1_normalizedVector{26};

%distancias al conjunto 1
for i = 1:length(c1_read)
    c1_distanceM{i} = pdist2(featuresIma,c1_normalizedVector{i},'minkowski');
    c1_distanceE{i} = pdist2(featuresIma,c1_normalizedVector{i},'euclidean');
end

%distancias al conjunto 2
for i = 1:length(c2_read)
    c2_distanceM{i} = pdist2(featuresIma,c2_normalizedVector{i},'minkowski');
    c2_distanceE{i} = pdist2(featuresIma,c2_normalizedVector{i},'euclidean');
end