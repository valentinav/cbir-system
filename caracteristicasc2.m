c2_read = dir('C:\Users\valen\Documents\2020.1\CBIR\practica2\conjunto2\*.jpg');

for i = 1:length(c2_read)
    c2I = c2_read(i).name;
    c2namei='C:\Users\valen\Documents\2020.1\CBIR\practica2\conjunto2\';
    c2_IM{i} = imread(strcat(c2namei,c2I));
end

for i = 1:length(c2_read)
    c2_R{i}= c2_IM{i}(:,:,1);
    c2_G{i}= c2_IM{i}(:,:,2);
    c2_B{i}= c2_IM{i}(:,:,3);

    %estadisticos de RGB
    c2_meanR{i}=mean2(c2_R{i});%media
    c2_meanG{i}=mean2(c2_G{i});
    c2_meanB{i}=mean2(c2_B{i});
    c2_stdR{i}=std2(c2_R{i}); %desviación estandar
    c2_stdG{i}=std2(c2_G{i});
    c2_stdB{i}=std2(c2_B{i});

    %obtención de estadísticas a partir de GLCM
    c2I=rgb2gray(c2_IM{i});
    glcms = graycomatrix(c2I);
    c2_staRGB{i} = graycoprops(glcms,{'all'});

    %elementos del vector de caracteristicas:
    c2_V1{i}=[c2_meanR{i} c2_meanG{i} c2_meanB{i} c2_stdR{i} c2_stdG{i} c2_stdB{i} c2_staRGB{i}.Contrast c2_staRGB{i}.Correlation c2_staRGB{i}.Energy c2_staRGB{i}.Homogeneity];

end

for i = 1:length(c2_read) 
    %estadisticos a partir de HSV
    c2_hsvIM{i} = rgb2hsv(c2_IM{i}); %convierte la imagen en HSV

    c2_H{i}= c2_hsvIM{i}(:,:,1);
    c2_S{i}= c2_hsvIM{i}(:,:,2);
    c2_V{i}= c2_hsvIM{i}(:,:,3);

    c2_meanH{i}=mean2(c2_H{i}); %media
    c2_meanS{i}=mean2(c2_S{i});
    c2_meanV{i}=mean2(c2_V{i});
    c2_stdH{i}=std2(c2_H{i}); %desviación estandar
    c2_stdS{i}=std2(c2_S{i});
    c2_stdV{i}=std2(c2_V{i});

    %obtención de estadísticas a partir de GLCM
    c2I=rgb2gray(c2_hsvIM{i});
    glcms = graycomatrix(c2I);
    c2_staHSV{i} = graycoprops(glcms,{'all'});

    c2_V2{i} = [c2_meanH{i}, c2_meanS{i}, c2_meanV{i}, c2_stdH{i}, c2_stdS{i}, c2_stdV{i} c2_staHSV{i}.Contrast c2_staHSV{i}.Correlation c2_staHSV{i}.Energy c2_staHSV{i}.Homogeneity];

end

for i = 1:length(c2_read)
   %vector de caracteristicas de la imagen
    c2_featureVector{i}=[c2_V1{i} c2_V2{i}];
    c2_normalizedVector{i} = c2_featureVector{i}/norm(c2_featureVector{i});
end
