source = imread('Culinary_fruits_front_view.jpg');
target = imread('orange.jpg');
source = rgb2gray(source);
target = rgb2gray(target);
  sourcePoints=detectSURFFeatures(source,'MetricThreshold',100.0,'NumOctaves',1,'NumScaleLevels',6);
 targetPoints=detectSURFFeatures(target,'MetricThreshold',100.0,'NumOctaves',1,'NumScaleLevels',6);
     [sourceFeatures,sourcePoints]=extractFeatures(source,sourcePoints,'SURFSize',64);
     [targetFeatures,targetPoints]=extractFeatures(target,targetPoints,'SURFSize',64);

  boxPairs = matchFeatures(sourceFeatures, targetFeatures);

  matchedSourcePoints = sourcePoints(boxPairs(:, 1), :);
  matchedTargetPoints = targetPoints(boxPairs(:, 2), :);

  % IS there any metric can used to make accurate decision if there is a match or not instead of percentage because its not accurate as required 
%   numPairs = length(boxpairs); %the number of pairs
%   percentage  = numPairs/100;
% 
%      if percentage >= 0.40
%      disp('We have this');
%        else
%      disp('We do not have this');
%      disp(percentage);
%     end