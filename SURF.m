%Dirk-Jan Kroon (2020). OpenSURF (including Image Warp) (https://www.mathworks.com/matlabcentral/fileexchange/28300-opensurf-including-image-warp), MATLAB Central File Exchange. Retrieved October 24, 2020.

path = 'C:\Users\valen\OneDrive\Documentos\MATLAB\SURF';
addpath([path '\SubFunctions']);

I1 = im2double(imread([path '\adidas1.jpg']));
%figure, imshow(I1);

%Generar la imagen integral
intgImg = IntegralImage_IntegralImage(I1);

%Ajustar opciones del algoritmo
Options.upright = false; %Rotación-> false: imágenes rotadas, true: imágenes no rotadas
Options.extended = false; %Descriptor false 64 y true 128
Options.verbose = false; %Información visual de filtros y keypoints 

FastHessianData.img = intgImg;
FastHessianData.thresh = 0.0001; %Umbral de la constante Hessiana para la detección de keypoints
FastHessianData.octaves = 5; %Número de pirámides
FastHessianData.init_sample = 2; %Número de capas dentro de cada octava

%Detectar y obtener los puntos de interés
Ipoits = FastHessian_getIpoints(FastHessianData,Options.verbose);

%Generar el descriptor de puntos de interés
Ipoits = SurfDescriptor_DecribeInterestPoints(Ipoits,Options.upright,Options.extended,intgImg,Options.verbose);
D1 = reshape([Ipoits.descriptor],64,[]);

%PROCESO IMAGEN 2

I2 = im2double(imread([path '\adidas2.jpg']));
figure, imshow(I2);
intgImg2 = IntegralImage_IntegralImage(I2);
FastHessianData.img = intgImg2;
Ipts2 = FastHessian_getIpoints(FastHessianData,Options.verbose);
Ipts2 = SurfDescriptor_DecribeInterestPoints(Ipts2,Options.upright,Options.extended,intgImg2,Options.verbose);
D2 = reshape([Ipts2.descriptor],64,[]);

%EMPAREJAMIENTO

%Encontrar las mejores coincidencias
err = zeros(1,length(Ipoits));
cor1=1:length(Ipoits);
cor2 = zeros(1,length(Ipoits));
for i=1:length(Ipoits)
    distance = sum((D2-repmat(D1(:,i),[1 length(Ipts2)])).^2,1);
    [err(i),cor2(i)] = min(distance);
end

%Ordenar las coincidencias
[err, ind] = sort(err);
cor1 = cor1(ind);
cor2 = cor2(ind);

%Generar vectores con información de las coordenadas de las mejores coincidencias
Pos1 = [[Ipoits(cor1).y]',[Ipoits(cor1).x]'];
Pos2 = [[Ipts2(cor2).y]',[Ipts2(cor2).x]'];
Pos1 = Pos1(1:30,:);
Pos2 = Pos2(1:30,:);

%Mostrar las dos imagenes
I = zeros([size(I1,1) size(I1,2)*2 size(I1,3)]);
I(:,1:size(I1,2),:) = I1; I(:,size(I1,2)+1:size(I1,2)+size(I2,2),:) = I2;
figure, imshow(I); hold on;

%Mostrar las mejores coincidencias
plot([Pos1(:,2) Pos2(:,2)+size(I1,2)]',[Pos1(:,1) Pos2(:,1)]','-');
plot([Pos1(:,2) Pos2(:,2)+size(I1,2)]',[Pos1(:,1) Pos2(:,1)]','o');
