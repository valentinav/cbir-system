CALVO = imread("C:\Users\valen\Documents\2020.1\CBIR\imagenesP5\calvo.JPG");
CALVO_GRIS = rgb2gray(CALVO);
% Para lo anterior, se deben tener en cuenta 2 variables:
NIVELES = 3;
WAVELET = 'db1';
% Se procede a descomponer la imagen...
[C, S] = wavedec2(CALVO_GRIS, NIVELES, WAVELET);
% NIVEL 1
NIVEL_AP = 1;
AP1 = appcoef2(C, S, WAVELET, NIVEL_AP);
 
% NIVEL 2
NIVEL_AP = 2;
AP2 = appcoef2(C, S, WAVELET, NIVEL_AP);
 
% NIVEL 3
NIVEL_AP = 3;
AP3 = appcoef2(C, S, WAVELET, NIVEL_AP);

% Una vez descargada la imagen...
FLOR = imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP5\flor.png');
% Para lo anterior, se deben tener en cuenta 2 variables:
NIVELES = 4;
WAVELET = 'db1';
% Se procede a descomponer la imagen...
[C, S] = wavedec2(FLOR, NIVELES, WAVELET);
% NIVEL 1
NIVEL = 1;
[H1,V1,D1] = detcoef2('all', C, S, NIVEL);
width = 400; height = 400;
 
M = randi([0, 2], [height, width]);
M(width / 2, height / 2) = 165;
M(width / 3, height / 3) = 165;
M(width / 2, height / 3) = 165;
A(width / 3, height / 2) = 165;
NIVELES = 1;
WAVELET = 'db1';
% Se procede a descomponer la imagen...
[C, S] = wavedec2(M, NIVELES, WAVELET);
 
NIVEL_AP = 1;
AP1 = appcoef2(C, S, WAVELET, NIVEL_AP);
% Funciones:
%   blocks
%   bumps
%   heavy sine
%   doppler
NOISE = 10;
FUNC = 'bumps';
[X,XN] = wnoise(FUNC, NOISE, sqrt(6));
subplot(211)
plot(X); title('Original Signal');
AX = gca;
subplot(212)
plot(XN); title('Noisy Signal');
AX = gca;
 
xd = wdenoise(XN,1);
figure;
plot(X,'b')
hold on;
plot(xd)
legend('Original Signal','Denoised Signal','Location','NorthEastOutside')
axis tight;
hold off;


