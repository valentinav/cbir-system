%A) construcción vector de caracteristicas
%imagen en RGB
IM = imread('C:\Users\valen\Documents\2020.1\CBIR\imagenesP2\c044.jpg');
IM_R= IM(:,:,1);
IM_G= IM(:,:,2);
IM_B= IM(:,:,3);

%B) Se extraen las caracteristicas de estadisticas media y desviación estandar
% (momentos de color)
meanR = mean2(IM_R);
meanG=mean2(IM_G);
meanB=mean2(IM_B);

stdR=std2(IM_R); %desviación estandar
stdG=std2(IM_G);
stdB=std2(IM_B);

IM_R1= double(IM(:,:,1));
skewnessR = skewness(IM_R1(:));

%elementos del vector de caracteristicas:
V1=[meanR meanG meanB stdR stdG stdB];

%C) estadisticos a partir de HSV
IM_hsvImage = rgb2hsv(IM); %convierte la imagen en HSV

meanHue = mean2(IM_hsvImage(:,:,1));
meanSat = mean2(IM_hsvImage(:,:,2));
meanValue = mean2(IM_hsvImage(:,:,3));

sdImage = stdfilt(IM_hsvImage(:,:,3)); %desviación del volumen
meanStdDev = mean2(sdImage);

%D) otros elementos para el vector
V2 = [meanHue, meanSat, meanValue, meanStdDev];
%vector de caracteristicas de la imagen 
IM_featureVector=[V1 V2];

% E) normalizar el vector de caracteristicas
normalizedVector = IM_featureVector/norm(IM_featureVector);

%F)
IG = mat2gray(IM);

IG_R= IG(:,:,1);
IG_G= IG(:,:,2);
IG_B= IG(:,:,3);

%Se extraen las caracteristicas de estadisticas media y desviación estandar
% (momentos de color)
IG_meanR= mean2(IG_R);
IG_meanG=mean2(IG_G);
IG_meanB=mean2(IG_B);

IG_stdR=std2(IG_R); %desviación estandar
IG_stdG=std2(IG_G);
IG_stdB=std2(IG_B);

IG_R1= double(IG(:,:,1));
IG_skewnessR= skewness(IG_R1(:));

%elementos del vector de caracteristicas:
IG_V1=[IG_meanR IG_meanG IG_meanB IG_stdR IG_stdG IG_stdB];

IG_hsvImage = rgb2hsv(IG); %convierte la imagen en HSV

IG_meanHue = mean2(IG_hsvImage(:,:,1));
IG_meanSat = mean2(IG_hsvImage(:,:,2));
IG_meanValue = mean2(IG_hsvImage(:,:,3));

IG_sdImage = stdfilt(IG_hsvImage(:,:,3)); %desviación del volumen
IG_meanStdDev = mean2(IG_sdImage);

IG_V2 = [IG_meanHue, IG_meanSat, IG_meanValue, IG_meanStdDev];

%vector de caracteristicas de la imagen 
IG_featureVector=[IG_V1 IG_V2];
%normalizar el vector de caracteristicas
IG_normalizedVector = IG_featureVector/norm(IG_featureVector);


