c1_read = dir('C:\Users\valen\Documents\2020.1\CBIR\practica2\conjunto1\*.jpg');

for i = 1:length(c1_read)
    c1I = c1_read(i).name;
    c1namei='C:\Users\valen\Documents\2020.1\CBIR\practica2\conjunto1\';
    c1_IM{i} = imread(strcat(c1namei,c1I));
end

for i = 1:length(c1_read)
    c1_R{i}= c1_IM{i}(:,:,1);
    c1_G{i}= c1_IM{i}(:,:,2);
    c1_B{i}= c1_IM{i}(:,:,3);

    %estadisticos de RGB
    c1_meanR{i}=mean2(c1_R{i});%media
    c1_meanG{i}=mean2(c1_G{i});
    c1_meanB{i}=mean2(c1_B{i});
    c1_stdR{i}=std2(c1_R{i}); %desviación estandar
    c1_stdG{i}=std2(c1_G{i});
    c1_stdB{i}=std2(c1_B{i});

    %obtención de estadísticas a partir de GLCM
    c1I=rgb2gray(c1_IM{i});
    glcms = graycomatrix(c1I);
    c1_staRGB{i} = graycoprops(glcms,{'all'});

    %elementos del vector de caracteristicas:
    c1_V1{i}=[c1_meanR{i} c1_meanG{i} c1_meanB{i} c1_stdR{i} c1_stdG{i} c1_stdB{i} c1_staRGB{i}.Contrast c1_staRGB{i}.Correlation c1_staRGB{i}.Energy c1_staRGB{i}.Homogeneity];

end

for i = 1:length(c1_read) 
    %estadisticos a partir de HSV
    c1_hsvIM{i} = rgb2hsv(c1_IM{i}); %convierte la imagen en HSV

    c1_H{i}= c1_hsvIM{i}(:,:,1);
    c1_S{i}= c1_hsvIM{i}(:,:,2);
    c1_V{i}= c1_hsvIM{i}(:,:,3);

    c1_meanH{i}=mean2(c1_H{i}); %media
    c1_meanS{i}=mean2(c1_S{i});
    c1_meanV{i}=mean2(c1_V{i});
    c1_stdH{i}=std2(c1_H{i}); %desviación estandar
    c1_stdS{i}=std2(c1_S{i});
    c1_stdV{i}=std2(c1_V{i});

    %obtención de estadísticas a partir de GLCM
    c1I=rgb2gray(c1_hsvIM{i});
    glcms = graycomatrix(c1I);
    c1_staHSV{i} = graycoprops(glcms,{'all'});

    c1_V2{i} = [c1_meanH{i}, c1_meanS{i}, c1_meanV{i}, c1_stdH{i}, c1_stdS{i}, c1_stdV{i} c1_staHSV{i}.Contrast c1_staHSV{i}.Correlation c1_staHSV{i}.Energy c1_staHSV{i}.Homogeneity];

end

for i = 1:length(c1_read)
   %vector de caracteristicas de la imagen
    c1_featureVector{i}=[c1_V1{i} c1_V2{i}];
    c1_normalizedVector{i} = c1_featureVector{i}/norm(c1_featureVector{i});
end
