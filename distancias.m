%scripts de previa ejecución
caracteristicasc1
caracteristicasc2

%primer imágen del conjunto 1
featuresIma = c1_normalizedVector{1};

%distancias al conjunto 1
for i = 2:length(c1_read)
    c1_distanceM{i} = pdist2(featuresIma,c1_normalizedVector{i},'minkowski');
    c1_distanceE{i} = pdist2(featuresIma,c1_normalizedVector{i},'euclidean');
end

%distancias al conjunto 2
for i = 1:length(c2_read)
    c2_distanceM{i} = pdist2(featuresIma,c2_normalizedVector{i},'minkowski');
    c2_distanceE{i} = pdist2(featuresIma,c2_normalizedVector{i},'euclidean');
end